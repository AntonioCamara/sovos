Feature: Choose a digital content
    As a user
    I want to access the content area
    So that I can choose a product

  Scenario: Choose a digital content
        Given I click on menu on main page
        When I see the content area
        And I click on content by Amazon Music
        Then I can choose a stream music as podcasts