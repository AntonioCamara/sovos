Feature: Choose a product by category
    As a user
    I want to access the category area
    So that I can buy a product

  Scenario: Buy a product on category area
        Given I access the menu on main page
        When I see the category area
        And I choose a product by category
        Then I can do the buy this product