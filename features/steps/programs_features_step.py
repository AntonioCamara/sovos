from behave import given, when, then
from pages.main_page import MainPage
from pages.found_on_amazon import FoundItOnAmazon

mainPage = MainPage()
foundItOnAmazon = FoundItOnAmazon()


@given('I click on main menu')
def step_impl(context):
    mainPage.access_main_page("https://www.amazon.com/")
    mainPage.click_main_menu()

@when('I click on feature by FoundItOnAmazon')
def step_impl(context):
    mainPage.click_program_features()

@then('I can see the page on Amazon')
def step_impl(context):
    if foundItOnAmazon.get_text_title() == "#FoundItOnAmazon":
        assert True