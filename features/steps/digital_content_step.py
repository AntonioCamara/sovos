from behave import given, when, then
from pages.main_page import MainPage
from pages.podcasts_page import podcasts

mainPage = MainPage()
podcasts = podcasts()


@given('I click on menu on main page')
def step_impl(context):
    mainPage.access_main_page("https://www.amazon.com/")
    mainPage.click_main_menu()


@when('I see the content area')
def step_impl(context):
    mainPage.click_content_menu_area()


@when('I click on content by Amazon Music')
def step_impl(context):
    mainPage.click_stream_music_area()

@then('I can choose a stream music as podcasts')
def step_impl(context):
    podcasts.click_on_btn_listenNow()
