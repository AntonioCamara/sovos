from behave import given, when, then
from pages.main_page import MainPage
from pages.camera_photo import cameraPhoto

mainPage = MainPage()
cp = cameraPhoto()

@given('I access the menu on main page')
def step_impl(context):
    mainPage.access_main_page("https://www.amazon.com/")


@when('I see the category area')
def step_impl(context):
    mainPage.click_main_menu()
    mainPage.wait_category_menu_area()
    mainPage.click_electronics_menu_area()
    mainPage.click_menu_electronics_option()
    if mainPage.validate_page_click() == "Camera & Photo":
        assert True


@when('I choose a product by category')
def step_impl(context):
    cp.choose_product()

@then('I can do the buy this product')
def step_impl(context):
    cp.click_on_btn_buyNow()
