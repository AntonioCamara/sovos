import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from browser import Browser

class FoundItOnAmazon(Browser):

    def get_text_title(self):
        title = self.driver.title
        time.sleep(2)
        return title