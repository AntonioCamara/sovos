import time
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from browser import Browser

class cameraPhoto(Browser):
    def choose_product(self):
        span = self.driver.find_elements_by_tag_name("span")
        for i in span:
            if i.text == "Wyze Cam v3 1080p HD Indoor/Outdoor Video Camera for Security, Pets, Baby Monitor, w/Color Night Vision, 2-Way Audio, Works with Alexa & Google":
                i.click()
                break

    def click_on_btn_buyNow(self):
        id_btn_buyNow = 'buy-now-button'
        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.ID, id_btn_buyNow)))
        self.driver.find_element_by_id(id_btn_buyNow).click()