import time
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from browser import Browser

class MainPage(Browser):
    def access_main_page(self, url):
        self.driver.get(url)

    def click_no_alert(self):
        WebDriverWait(self.driver, 10).until(EC.alert_is_present())
        self.driver.switch_to.alert.dismiss()

    def click_main_menu(self):
        self.driver.refresh()
        time.sleep(3)
        id_main_menu = 'nav-hamburger-menu'
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, id_main_menu)))
        self.driver.find_element_by_id(id_main_menu).click()

    def wait_category_menu_area(self):
        xpath_main_menu = '//*[@id="hmenu-content"]/ul[1]/li[6]/div'
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath_main_menu)))

    def click_program_features(self):
        #FoundItOnAmazon
        xpath_program_features = '//*[@id="hmenu-content"]/ul[1]/li[15]/a'
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath_program_features)))
        self.driver.find_element_by_xpath(xpath_program_features)

    def click_content_menu_area(self):
        #Amazon Music
        xpath_content_area = "//*[@id='hmenu-content']/ul[1]/li[2]/a/div"
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath_content_area)))
        self.driver.find_element_by_xpath(xpath_content_area).click()

    def click_stream_music_area(self):
        #Podcasts
        xpath_stream_music_area = '//*[@id="hmenu-content"]/ul[2]/li[7]/a'
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath_stream_music_area)))
        self.driver.find_element_by_xpath(xpath_stream_music_area).click()

    def click_electronics_menu_area(self):
        xpath_menu_electronics = '//*[@id="hmenu-content"]/ul[1]/li[7]/a/div'
        self.driver.find_element_by_xpath(xpath_menu_electronics).click()

    def wait_electronics_option(self):
        xpath_elec_options = '//*[@id="hmenu-content"]/ul[5]/li[2]/div'
        WebDriverWait(self, 10).until(EC.presence_of_element_located((By.XPATH, xpath_elec_options)))

    def click_menu_electronics_option(self):
        xpath_camera_photo = '//*[@id="hmenu-content"]/ul[5]/li[4]/a'
        time.sleep(2)
        #WebDriverWait(self, 10).until(EC.presence_of_element_located((By.XPATH, xpath_camera_photo)))
        elem = self.driver.find_element_by_xpath(xpath_camera_photo)
        #self.driver.execute_script("arguments[0].click();", elem)
        action = ActionChains(self.driver)
        action.move_to_element(elem).click().perform()

        #self.driver.find_element_by_xpath(xpath_camera_photo).click()
        time.sleep(2)

    def validate_page_click(self):
        css_selector = '#nav-search-label-id'
        text = self.driver.find_element_by_css_selector(css_selector).text
        return text


