import time
import base64
from uuid import UUID
import xml.etree.cElementTree as ET
import requests

#should return notifications for the following countries: BR, AR
def test_get_return_notifications():
    headers = {
        'Accept': '*/*',
        'User-Agent': 'request',
    }
    url = 'https://mocki.io/v1/19430625-2b1c-492a-925f-8b4921964ac3'
    response = requests.get(url, headers=headers)
    response_dict = response.json()
    data_notifications = response_dict['data']['notifications']
    list_notifications = []
    for i in data_notifications:
        country = i['metadata']['country']
        if country == "BR" or country == "AR":
            list_notifications.append(i['notificationId'])
            assert True
        else:
            assert False

#perPage value should correspond to the number of notifications retrieved
def test_get_value_perPage():
    headers = {
        'Accept': '*/*',
        'User-Agent': 'request',
    }
    url = 'https://mocki.io/v1/19430625-2b1c-492a-925f-8b4921964ac3'
    response = requests.get(url, headers=headers)
    response_dict = response.json()

    number_perPage = response_dict['data']['pageState']['perPage']
    data_notifications = response_dict['data']['notifications']
    list_notifications = []
    for i in data_notifications:
        list_notifications.append(i['notificationId'])

    if len(list_notifications) == number_perPage:
        assert True

#content of notifications should be a xml encoded on Base64
def test_content_notifications_xml_base64():
    headers = {
        'Accept': '*/*',
        'User-Agent': 'request',
    }
    url = 'https://mocki.io/v1/19430625-2b1c-492a-925f-8b4921964ac3'
    response = requests.get(url, headers=headers)
    response_dict = response.json()
    data_notifications = response_dict['data']['notifications']
    count = 1
    for i in data_notifications:
        encoded = i['content']
        # add content que vem da API
        base64_string = encoded
        base64_bytes = base64_string.encode("ascii")

        sample_string_bytes = base64.b64decode(base64_bytes)
        sample_string = sample_string_bytes.decode("ascii")
        # print(sample_string)

        root = ET.Element(sample_string)
        tree = ET.ElementTree(root)
        tree.write('test' + str(count) + '.xml')

        time.sleep(1)
        file = open('test' + str(count) + '.xml', 'wb')
        file.write(str(sample_string).encode())
        file.close()
        count += 1


#notificationId should be a valid GUID
def test_notificationID_is_valid_guid():
    headers = {
        'Accept': '*/*',
        'User-Agent': 'request',
    }
    url = 'https://mocki.io/v1/19430625-2b1c-492a-925f-8b4921964ac3'
    response = requests.get(url, headers=headers)
    response_dict = response.json()

    data_notifications = response_dict['data']['notifications']
    list_notifications = []
    for i in data_notifications:
        list_notifications.append(i['notificationId'])

        try:
            uuid_obj = UUID(i['notificationId'])
            if str(uuid_obj) == i['notificationId']:
                assert True
        except ValueError:
            assert False


#notificationId should correspond to ID inside content xml document
def test_notificationId_with_ID_inside_xml():
    #pegar notificationID da api e comparar com notificationID do xml salvo
    headers = {
        'Accept': '*/*',
        'User-Agent': 'request',
    }
    url = 'https://mocki.io/v1/19430625-2b1c-492a-925f-8b4921964ac3'
    response = requests.get(url, headers=headers)
    response_dict = response.json()

    data_notifications = response_dict['data']['notifications']
    list_notificationsID = []
    for i in data_notifications:
        list_notificationsID.append(i['notificationId'])

    #comparar com os arquivos xmls salvos do teste anterior para fazer um match
    list_notificationsID_xml = []
    for i in range(1, 6):
        tree = ET.parse('test'+str(i)+'.xml')
        root = tree.getroot()
        ID_xml = root[1].text
        list_notificationsID_xml.append(ID_xml)

    for i in range(len(list_notificationsID)):
        if list_notificationsID_xml[i] == list_notificationsID[i]:
           assert True
        else:
            assert False

#200 notifications should have "Document Authorized" on StatusReason
#and "Document authorized successfully" on Text fields inside content xml document

#400 notifications should have "Document Rejected" on StatusReason and
#"Document was rejected by tax authority" on Text fields inside content xml document

def test_notifications_StatusReason_content_XML():
    for i in range(1, 6):
        tree = ET.parse('test' + str(i) + '.xml')
        root = tree.getroot()
        time.sleep(1)
        status_tree = root[3][0][0]
        statusReasonCode = status_tree[0].text
        statusReason = status_tree[1].text
        statusText = status_tree[2].text

        if statusReasonCode == "200":
            if statusReason == "Document Authorized" and statusText == "Document authorized successfully":
                assert True
            else:
                assert False
        if statusReasonCode == "400":
            if statusReason == "Document Rejected" and statusText == "Document was rejected by tax authority":
                assert True
            else:
                assert False