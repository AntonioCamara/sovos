from browser import Browser


def before_all(context):
    context.driver = Browser()

def after_all(context):
    context.driver.browser_quit()

def before_scenario(context, scenario):
    context.driver = Browser

def after_scenario(context, scenario):
    context.driver.browser_quit()